<?php 
	require_once('function.php');
	if ((empty($_GET)) || (!isset($_GET['tableName'])) || (!isset($_GET['change']))) {
		if (empty($_POST)) {
			showError403();
		}
		
	}
	$field = $_GET['change'];
	$type = $_GET['type'];
	if ((!empty($_POST)) && (isset($_POST['changeName']))) {
	$newField = $_POST['changeName'];
	$tableName = $_POST['tableName'];
	$field = $_POST['field'];
	$type = $_POST['type'];
	if (!empty($_POST['changeType'])) {
		$type = $_POST['changeType'];
	}
	    $pdo = create_pdo();
    	$sql = "ALTER TABLE $tableName CHANGE $field $newField $type";
    	$pdo->exec($sql);
    	header('location: oneTable.php?tableName='.$tableName);  
	}

 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Document</title>
 </head>
 <body>
 	<h1>Изменение поля <?=$field?></h1>
 	<h3>изменение имя поля:</h3>
 	<form action="changeField.php" method="POST">
 		<input type="hidden" name="tableName" value=<?=$_GET['tableName']?>>
 		<input type="hidden" name="field" value=<?=$_GET['change']?>>
 		<input type="hidden" name="type" value=<?=$_GET['type']?>>
 		новое имя: <input name="changeName" type="text" required><br><br>
 		изменить тип: <input name="changeType" type="text">
 		<input type="submit" value="Изменить">
 	</form>
 </body>
 </html>