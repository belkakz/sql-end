<?php 

function create_pdo(){
	$pdo = new PDO ("mysql:host=localhost;dbname=shesterin;charset=utf8", "shesterin", "neto1766");
	return $pdo;
}
//Показать все таблицы
function showAllTables() {
	$pdo = create_pdo();
	$sql = "SHOW TABLES";
    $sth=$pdo->prepare($sql);
    $sth->execute([]);
    $base=$sth->fetchAll(PDO::FETCH_ASSOC);
    return $base;
}
//Показать конкретную таблицу
function showTable($tablename) {
	$pdo = create_pdo();
	$sql = "DESCRIBE $tablename";
    $sth=$pdo->prepare($sql);
    $sth->execute([]);
    $base=$sth->fetchAll(PDO::FETCH_ASSOC);
    return $base;
}

//удаление таблицы
function dropTable($table) {
	$pdo = create_pdo();
	$sql = "SHOW TABLES";
    $sth=$pdo->prepare($sql);
    $sth->execute([]);
    $base=$sth->fetchAll(PDO::FETCH_ASSOC);
    $i = 0;
    //Проверка на существование
    foreach ($base as $key => $value) {
    	if ($value['Tables_in_shesterin'] === $table) {
    		$i++;
    	}
    }
    	if ($i === 0) {
    		echo "Нет такой таблицы";
    		return false;
    	}
    	$pdo = create_pdo();
    	$sql = "DROP TABLE IF EXISTS $table";
    	$pdo->exec($sql);
    	echo "Таблица удалена!";
}
//Вывод существующих таблиц
function printTables() {
		$base = showAllTables();
	echo '<table>
		<tr>
			<th>Название таблицы</th>
			<th>Удалить таблицу</th>
		</tr>';
	foreach ($base as $value) {

		echo '<tr><td><a href="oneTable.php?tableName='.$value['Tables_in_shesterin'].'">'.$value['Tables_in_shesterin'].'</a></td>';
		echo '<td><a style="color:red;" href="index.php?tableName='.$value['Tables_in_shesterin'].'&dropTable=1">Удалить</a></td></tr>';
	}
	echo '</table>';
}
//удаление поля конкретной таблицы
function deleteField($tableName, $fieldName) {
	$pdo = create_pdo();
	$sql = "DESCRIBE $tableName";
    $sth=$pdo->prepare($sql);
    $sth->execute([]);
    $base=$sth->fetchAll(PDO::FETCH_ASSOC);
    $i = 0;
    foreach ($base as $key => $value) {
    	if ($value['Field'] === $fieldName) {
    		$i++;
    	}
    }
    	if ($i === 0) {
    		echo "Нет такого поля";
    		return false;
    	}
    $pdo = create_pdo();
    $sql = "ALTER TABLE $tableName DROP COLUMN $fieldName";
    $pdo->exec($sql);
    header('location: oneTable.php?tableName='.$tableName.'&deleteOk=1');  
}
//Создать таблицу
function createTable($tableName) {
	$pdo = create_pdo();
	$sql = "SHOW TABLES WHERE Tables_in_shesterin='$tableName'";
   $sth=$pdo->prepare($sql);
   $sth->execute([]);
   $base=$sth->fetchAll(PDO::FETCH_ASSOC);
   if (empty($base)) {
		$pdo = create_pdo();
		$sql = "CREATE TABLE `$tableName` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `age` int(11) DEFAULT NULL,
	  `name` varchar(150) NOT NULL,
	  `date_added` timestamp NULL DEFAULT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";

	 				$pdo->exec($sql);
	 				echo "Таблица создана";
   }	else echo 'Такая таблица уже есть!';


}

function showError403() {
  	  	http_response_code(403);
   		exit('<h1>403 Forbidden</h1><p>Перейти на <a href="index.php">главную</a></p>');
	}

function showError404() {
  	  	http_response_code(404);
   		exit('<h1>403 Forbidden</h1><p>Перейти на <a href="index.php">главную</a></p>');
	}


 ?> 