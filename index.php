<?php 
require_once('function.php');
$pdo = create_pdo();
   $sql = "SHOW TABLES";
   $sth=$pdo->prepare($sql);
   $sth->execute([]);
   $myTask=$sth->fetchAll(PDO::FETCH_ASSOC);

   if ((!empty($_GET)) && (isset($_GET['dropTable']))) {
   	dropTable($_GET['tableName']);
   }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Изменение табличек</title>
	<style>
		th{
 			background: #c4c4c4;
 		}
 		td, th {
 			border: 1px solid #dfe2e5;
			padding: 6px 13px;
			text-align: center;
 		}
	</style>
</head>
<body>
	<h1>создать таблицу в БД</h1>
	<form action="index.php" method="GET">
		<label for="">Введите имя таблички латиницей, без пробелов
			<input name="name" type="text">			
		</label>
		<input type="hidden" name="create" value="1">
		<input type="submit" value="Создать таблицу">
	</form>
	
		<?php
			if ((!empty($_GET)) && (isset($_GET['allTables']))) {
				
				printTables();				
				echo '<a href="index.php">Скрыть таблицы</a>';
			}

		 ?>
					
		
	
	
	<?php 
if (!isset($_GET['allTables'])) {
	echo '<a href="index.php?allTables=1">Показать все таблицы</a>';
}

if ((!empty($_GET)) && (isset($_GET['create']))) {
	createTable($_GET['name']);
}


	?>
</body>
</html>