<?php 
require_once('function.php');
	if ((!empty($_GET)) && (isset($_GET['tableName']))) {
		$table = $_GET['tableName'];
		$base = showTable($table);
	} else showError403();

	if ((!empty($_GET)) && (isset($_GET['delete']))) {
		deleteField($_GET['tableName'], $_GET['delete']);
	}
	
	if ((!empty($_GET)) && (isset($_GET['deleteOk']))) {
		echo 'Поле удалено';
	}
	
 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Табличка <?=$_GET['tableName']?></title>
 		<style>
		th{
 			background: #c4c4c4;
 		}
 		td, th {
 			border: 1px solid #dfe2e5;
			padding: 6px 13px;
			text-align: center;
 		}
	</style>
 </head>
 <body>
 	<h1>Данные таблицы <?=$_GET['tableName']?>:</h1>
 	<table>
 		<tr>
 			<th>Название поля</th>
 			<th>Тип</th>
 			<th>Удалить</th>
 			<th>Изменить</th>
 		</tr>
 		<?php 
 			foreach ($base as $value) {
 				echo '<tr><td>'.$value['Field'].'</td><td>'.$value['Type'].'</td><td><a href="oneTable.php?tableName='.$table.'&delete='.$value['Field'].'" style="color:red;">Удалить</a></td><td><a href="changeField.php?tableName='.$table.'&change='.$value['Field'].'&type='.$value['Type'].'">Изменить</a></td></tr>';
 			}
 		 ?>
 	</table>
 	<a href="index.php">На главную</a>
 	<a href="index.php?allTables=1">Назад ко всем таблицам</a>
 </body>
 </html>